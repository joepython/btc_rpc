
import datetime
import json

from flask import render_template, redirect, url_for, flash
from . import app
from app import bitcoin


peer_time = datetime.datetime.utcnow()

print('Getting peer info ...')
peer_info = bitcoin.getpeerinfo()
print('Peer info initiated.')


@app.route('/')
def index():
    this_text = ''
    this_text += 'Examine node and network data available using btc-cli API.'
    return render_template("base.html", title='Check Bitcoin Full Node', text_output=this_text)



@app.route('/raw/blockchaininfo')
def getblockchaininfo():
    x = bitcoin.getblockchaininfo()
    return render_template("raw_info.html", title='Raw BlockchainInfo',
                           text_output=json.dumps(x, indent=4))

@app.route('/raw/difficulty')
def difficulty():
    x = bitcoin.getdifficulty()
    return render_template("raw_info.html", title='Raw Difficulty',
                           text_output=json.dumps(x, indent=4))

@app.route('/raw/mempool')
def raw_mempool():
    x = bitcoin.getmempoolinfo()
    return render_template('raw_info.html', title='Raw Mempool',
                           text_output=json.dumps(x, indent=4))

@app.route('/raw/mininginfo')
def raw_mininginfo():
    x = bitcoin.getmininginfo()
    return render_template('raw_info.html', title='Raw Mining Info',
                           text_output=json.dumps(x, indent=4))


@app.route('/raw/networkinfo')
def networkinfo():
    x = bitcoin.getnetworkinfo()
    return render_template("raw_info.html", title='Raw NetworkInfo',
                           text_output=json.dumps(x, indent=4))


@app.route('/raw/peers')
def peers():
    stale, scalars = fetch_peer_info()
    flash(f'Peer data is > {stale.seconds} seconds old.')
    return render_template("raw_peers.html", title='Raw Peers',
                           text_output=json.dumps(peer_info, indent=4),
                           stale=stale, peers=peer_info,
                           scalars=scalars)


@app.route('/peers_io')
def peers_a():
    stale, scalars = fetch_peer_info()
    flash(f'Peer data is > {stale.seconds} seconds old.')
    return render_template('peers_a.html', title='InOutBound',
                           stale=stale, peers=peer_info,
                           scalars=scalars)


@app.route('/peers_refresh')
def peers_refresh():
    refresh_peers()
    scalars = peer_scalars(peer_info)
    flash(f'Manual refresh of peer info completed.')
    return redirect(url_for('index'))

def fetch_peer_info():
    print(f'start getpeerinfo() {datetime.datetime.utcnow()}')
    stale = datetime.datetime.utcnow() - peer_time
    print(f'stale = {stale} type={type(stale)}')
    if stale > app.config['PEERS_INTERVAL']:
        refresh_peers()
        x = datetime.datetime.utcnow()
        stale = x - x
    else:
        print('.... skip getting peer info ....')
    print(f'complete getpeerinfo() {datetime.datetime.utcnow()}')
    scalars = peer_scalars(peer_info)
    return stale, scalars


def refresh_peers():
    global peer_info
    global peer_time
    peer_info = bitcoin.getpeerinfo()
    peer_time = datetime.datetime.utcnow()
    return

def peer_scalars(peers):
    plen = len(peers)
    inbound = 0
    for peer in peers:
        if peer['inbound']:
            inbound += 1
    outbound = plen - inbound
    return (plen, inbound, outbound)

