
import datetime

from slickrpc import Proxy

from flask import Flask

app = Flask(__name__, instance_relative_config=True)

app.config.from_pyfile('config.py')

for x in app.config:
    print(f'{x} = {app.config[x]}')

rpc_ip = app.config['RPC_IP']
rpcuser = app.config['RPCUSER']
rpcpassword = app.config['RPCPASSWORD']

bitcoin = Proxy(f"http://{rpcuser}:{rpcpassword}@{rpc_ip}:8332")
